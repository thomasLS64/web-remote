const notifier = require('node-notifier')

const Notification = {

  notify: function(obj) {
    notifier.notify(obj)
  },

  message: function(title, msg, icon) {
    let obj = {
      title: title,
      message: msg
    }

    if (icon) {
      obj.icon = icon
    }

    Notification.notify(obj)
  },

  quickMessage: function(msg, icon) {
    let obj = {
      title: msg,
      message: ' ',
      timeout: 1
    }

    if (icon) {
      obj.icon = icon
    }

    Notification.notify(obj)
  }

}

module.exports = Notification