const CryptoJS = require('crypto-js') 

module.exports = {

  hash(str) {
    // console.log(CryptoJS)
    return CryptoJS.enc.Base64.stringify(CryptoJS.SHA256(str))
  }

}