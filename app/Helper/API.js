const API = {

  getBaseUrl() {
    let url = window.location.origin.split(':')
    
    url.pop()
    url = url.join(':')

    return url
  },

  async fetchWebServer(obj) {
    const opts = {
      method: obj.method ? obj.method : 'GET'
    }

    if (obj.body) {
      opts.body = JSON.stringify(obj.body)
    }

    return await fetch(API.getBaseUrl() + ':8081' + obj.path, opts)
  }

}

module.exports = API