const fs = require('fs')

const authenticated = {}

module.exports = {

  check: function(credential) {
    const users = JSON.parse(fs.readFileSync('storage/users.json', 'utf8'))
    const apps = JSON.parse(fs.readFileSync('storage/apps.json', 'utf8'))
  
    if (users[credential.username] && users[credential.username].password === credential.password) {
      const user = {
        role: users[credential.username].role,
        username: credential.username,
        firstConnexion: users[credential.username].firstConnexion,
        apps: []
      }
  
      if (users[credential.username].apps) {
        user.apps = user.apps.concat(users[credential.username].apps)
      }
      if (apps) {
        user.apps = user.apps.concat(apps)
      }
  
      if ('admin' === user.role) {
        user.users = []
        Object.keys(users).forEach(username => {
          user.users.push({
            username: username,
            role: users[username].role
          })
        })
      }
  
      return user
    }
  
    return false
  },

  generateRandomKey() {
    return Math.random().toString(36).substr(2) + '-' + Math.random().toString(36).substr(2) + '-' + Math.random().toString(36).substr(2) + '-' + Math.random().toString(36).substr(2)
  },

  connect: function(user) {
    const users = JSON.parse(fs.readFileSync('storage/users.json', 'utf8'))

    if (users[user.username]) {
      const key = this.generateRandomKey()

      authenticated[ key ] = user

      return key
    }

    return false
  },

  isConnected: function(key) {
    if (authenticated[key]) {
      return authenticated[key]
    }

    return false
  }

}