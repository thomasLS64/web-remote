const os = require('os')
const { exec } = require('child_process');

// Get Server IP information
const interfaces = os.networkInterfaces()
let addressesIPv4 = []
for (let inter in interfaces) {
  addressesIPv4 = addressesIPv4.concat(interfaces[inter].filter(network => 'IPv4' === network.family))
}
const localAddresses = addressesIPv4.filter(addr => addr.internal)
const externalAddresses = addressesIPv4.filter(addr => !addr.internal)

// Export
module.exports = {
  general: {
    arch: os.arch(),
    hostname: os.hostname(),
    platform: os.platform(),
    release: os.release(),
    type: os.type(),
    uptime: os.uptime()
  },
  cpus: os.cpus(),
  mem: {
    free: os.freemem(),
    total: os.totalmem()
  },
  network: {
    interfaces: interfaces,
    ipv4: {
      local: localAddresses,
      external: externalAddresses
    }
  },
  exec(cmd, fn) {
    exec(cmd, fn)
  }
}