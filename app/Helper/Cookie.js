module.exports = {

  parse: function(req) {
    const cookies = {}

    if (req.headers.cookie) {
      const splited = req.headers.cookie.split(';')
  
      splited.forEach(cookie => {
        const sCookie = cookie.split('=')
        cookies[ sCookie[0] ] = sCookie[1]
      })
    }

    return cookies
  }

}