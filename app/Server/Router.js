const ApiV1Controller = require('../Controller/ApiV1Controller.js')

const routes = {
  apiV1: {
    path: '/api/v1',
    exec: function(req, res) {
      res.setHeader('Access-Control-Allow-Origin', '*')
      res.setHeader('Access-Control-Request-Method', '*')
      res.setHeader('Access-Control-Allow-Methods', 'OPTIONS, GET, POST, PUT, PATCH')
      res.setHeader('Access-Control-Allow-Headers', '*')
    },
    routes: [
      {
        path: '/auth',
        method: 'POST',
        exec: ApiV1Controller.postAuthentication
      },
      {
        path: '/user',
        method: 'POST',
        exec: ApiV1Controller.postNewUser
      },
      {
        path: '/update-password',
        method: 'POST',
        exec: ApiV1Controller.updatePassword
      },
      {
        path: '/send',
        method: 'POST',
        exec: ApiV1Controller.postCommand
      },
      {
        path: '/os',
        method: 'GET',
        exec: ApiV1Controller.getNodeInformation
      },
      {
        path: '/app',
        method: 'POST',
        exec: ApiV1Controller.postNewShotcut
      },
      {
        path: '*',
        exec: function(req, res) {
          res.writeHead(404, { 'Content-Type': 'application/json' })
          res.end(JSON.stringify({
            status: 404,
            error: 'Not found'
          }))
        }
      }
    ]
  },
  web: {
    path: '*',
    exec: require('../Controller/WebAppController.js').getHome
  }
}

function findAndExecController(req, res, current, path='') {
  let found = false

  if (current.path) {
    path += current.path

    if ((path.includes('*') || req.url.includes(path)) && (!current.method || (current.method === req.method))) {
      found = true

      if (current.exec) {
        current.exec(req, res)
      }

      if (current.routes) {
        for (let i = 0; i < current.routes.length; i++) {
          if (findAndExecController(req, res, current.routes[i], path)) {
            return true
          }
        }
      }
    }
  } else {
    for (let key in current) {
      if (findAndExecController(req, res, current[key], path)) {
        return true
      }
    }
  }

  return found
}

module.exports = {

  exec: function(req, res) {
    findAndExecController(req, res, routes)
  }

}