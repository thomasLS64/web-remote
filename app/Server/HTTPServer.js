const http = require('http')
const Router = require('./Router.js')

module.exports = function(opts) {
  // Create an HTTP server
  const server = http.createServer((req, res) => {
    Router.exec(req, res)
  })
  
  // HTTP server listening
  const os = require('../Helper/OS.js')
  server.listen(opts.port, opts.host, () => {
    console.log('Remote listening at:')
  
    if ('0.0.0.0' === opts.host) {
      os.network.ipv4.local.forEach((addr, i) => {
          console.log('- Local' + (os.network.ipv4.local.length > 1 ? '[' + i + ']' : '') + ":\thttp://" + addr.address + (opts.port != 80 ? ':' + opts.port + '/' : '/'))
      })
  
      os.network.ipv4.external.forEach((addr, i) => {
        console.log('- LAN' + (os.network.ipv4.external.length > 1 ? '[' + i + ']' : '') + ":\t\thttp://" + addr.address + (opts.port != 80 ? ':' + opts.port + '/' : '/'))
      })
    } else {
      console.log("- LAN:\t\thttp://" + opts.host + (opts.port != 80 ? ':' + opts.port + '/' : '/'))
    }
  })
}
