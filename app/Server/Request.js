module.exports = {

  buffer: function(req) {
    return new Promise((resolve, reject) => {
      const chunks = []

      req.on('data', chunk => chunks.push(chunk))

      req.on('end', () => {
        resolve(Buffer.concat(chunks))
      })

      req.on('error', (err) => {
        reject(err)
      })
    })
  },

  json: function(req) {
    return this.buffer(req).then(data => JSON.parse(data))
  }

}