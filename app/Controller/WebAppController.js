const fs = require('fs')

module.exports = {

  getHome: function(req, res) {
    const url = req.url === '/' || req.url.split('.').length === 1 ? '/index.html' : req.url

    fs.readFile('dist' + url, function (err, data) {
      if (err) {
        res.writeHead(404)
        res.end(JSON.stringify(err))
        return
      }

      res.writeHead(200)
      res.end(data)
    })
  }

}