const fs = require('fs')
const Authentication = require('../Helper/Authentication.js')
const Crypto = require('../Helper/Crypto.js')
const Request = require('../Server/Request.js')
const Cookie = require('../Helper/Cookie.js')

module.exports = {

  postAuthentication: function(req, res) {
    Request.json(req)
      .then(json => {
        const user = Authentication.check(json)

        if (user) {
          res.writeHead(200, {
            'Set-Cookie': 'auth=' + Authentication.connect(user),
            'Content-Type': 'application/json'
          });
          res.end(JSON.stringify({
            status: 200,
            data: user
          }))
        } else {
          res.writeHead(404, { 'Content-Type': 'text/plain' });
          res.end('Not Found');
        }
      })
  },

  postNewUser: function(req, res) {
    Request.json(req)
      .then(json => {
        const users = JSON.parse(fs.readFileSync('storage/users.json', 'utf8'))

        if (!(json && json.username)) {
          res.writeHead(400, { 'Content-Type': 'application/json' })
          res.end(JSON.stringify({
            status: 400,
            error: 'Username not defined'
          }))
        }
        else if (users[json.username]) {
          res.writeHead(400, { 'Content-Type': 'application/json' })
          res.end(JSON.stringify({
            status: 400,
            error: 'User already exist'
          }))
        }
        else {
          users[json.username] = {}
          users[json.username].password = Crypto.hash('password')
          users[json.username].role = json.role
          users[json.username].firstConnexion = true

          fs.writeFileSync('storage/users.json', JSON.stringify(users))
          res.writeHead(200, { 'Content-Type': 'application/json' })
          res.end(JSON.stringify({
            status: 200,
            user: {
              username: json.username,
              role: json.role
            }
          }))
        }
      })
  },

  updatePassword: function(req, res) {
    Request.json(req)
      .then(json => {
        const users = JSON.parse(fs.readFileSync('storage/users.json', 'utf8'))

        if (json && json.username && json.password === users[json.username].password) {
          users[json.username].password = json.newPassword
          users[json.username].firstConnexion = false

          fs.writeFileSync('storage/users.json', JSON.stringify(users))
          res.writeHead(200, { 'Content-Type': 'application/json' })
          res.end(JSON.stringify({
            status: 200
          }))
        } else {
          res.writeHead(404, { 'Content-Type': 'text/plain' })
          res.end('Not Found')
        }
      })
  },

  postCommand: function(req, res) {
    Request.json(req)
      .then(json => {
        if (
          require('../IO/Mouse.js')(req, res, json)
          || require('../IO/Keyboard.js')(req, res, json)
          || require('../IO/Command.js')(req, res, json)
        ) {
          res.writeHead(200, { 'Content-Type': 'application/json' });
          res.end(JSON.stringify({
            status: 200,
            msg: 'success'
          }))
        } else {
          res.writeHead(404, { 'Content-Type': 'text/plain' });
          res.end('Not Found');
        }
      })
  },

  getNodeInformation: function(req, res) {
    res.writeHead(200, { 'Content-Type': 'application/json' });
    res.end(JSON.stringify({
      status: 200,
      data: require('../Helper/OS.js')
    }))
  },

  postNewShotcut: function(req, res) {
    Request.json(req)
      .then(json => {
        console.log('Datas', json)
        if (json && json.type && json.iconType) {
          const newApp = json
          // TODO - Verify app construction

          if (json.shared) {
            delete json.shared
            const apps = JSON.parse(fs.readFileSync('storage/apps.json', 'utf8'))

            apps.push(newApp)
            fs.writeFileSync('storage/apps.json', JSON.stringify(apps))
            res.writeHead(200, { 'Content-Type': 'application/json' });
            res.end(JSON.stringify({
              status: 200,
              app: newApp
            }))
          } else {
            const cookies = Cookie.parse(req)
            const user = Authentication.isConnected(cookies.auth)
            const users = JSON.parse(fs.readFileSync('storage/users.json', 'utf8'))

            if (user && users[user.username]) {
              if (!users[user.username].apps) {
                users[user.username].apps = []
              }

              users[user.username].apps.push(newApp)
              fs.writeFileSync('storage/users.json', JSON.stringify(users))
              res.writeHead(200, { 'Content-Type': 'application/json' });
              res.end(JSON.stringify({
                status: 200,
                app: newApp
              }))
            } else {
              res.writeHead(401, { 'Content-Type': 'text/plain' });
              res.end('Unauthorized');
            }
          }
        } else {
          res.writeHead(404, { 'Content-Type': 'text/plain' });
          res.end('Not Found');
        }
      })
  }

}