const os = require('../Helper/OS')

module.exports = function (req, res, json) {
  if (json.exec) {
    os.exec(json.exec, function(err, stdout, stderr) {
      console.log('Result command', err, stdout, stderr)
    })

    return true
  }

  return false
}