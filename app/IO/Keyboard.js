const robot = require('robotjs')
const os = require('../Helper/OS.js')
const Notification = require('../Helper/Notification.js')

function tapKey(key, combo) {
  if (combo && combo.length > 0) {
    robot.keyTap(key, combo)
  } else {
    robot.keyTap(key)
  }
}

module.exports = function (req, res, json) {
  if (json.label) {
    switch (json.label) {
      case 'backspace':
        tapKey('backspace', json.combo)
        return true
      case 'enter':
        tapKey('enter', json.combo)
        return true
      case 'left':
        tapKey('left', json.combo)
        return true
      case 'up':
        tapKey('up', json.combo)
        return true
      case 'right':
        tapKey('right', json.combo)
        return true
      case 'down':
        tapKey('down', json.combo)
        return true
      case 'command':
        tapKey('command', json.combo)
        return true
      case 'vol_up':
        tapKey('audio_vol_up', json.combo)
        Notification.quickMessage('Vol+')
        return true
      case 'vol_down':
        tapKey('audio_vol_down', json.combo)
        Notification.quickMessage('Vol-')
        return true
      case 'play':
        tapKey('audio_play', json.combo)
        Notification.quickMessage('Play')
        return true
      case 'pause':
        tapKey('audio_pause', json.combo)
        Notification.quickMessage('Pause')
        return true
      case 'rewind':
        tapKey('audio_rewind', json.combo)
        Notification.quickMessage('Rewind')
        return true
      case 'forward':
        tapKey('audio_forward', json.combo)
        Notification.quickMessage('Forward')
        return true
      case 'lights_mon_up':
        tapKey('lights_mon_up', json.combo)
        Notification.quickMessage('Light+')
        return true
      case 'lights_mon_down':
        tapKey('lights_mon_down', json.combo)
        Notification.quickMessage('Light-')
        return true
      case 'win_close':
        if ('darwin' === os.general.platform) {
          tapKey('q', 'command')
        } else {
          tapKey('f4', 'alt')
        }
        Notification.quickMessage('Close')
        return true
      case 'backward':
        if ('darwin' === os.general.platform) {
          tapKey('left', 'command')
        } else {
          tapKey('left', 'alt')
        }
        Notification.quickMessage('Backward')
        return true
      default:
        if (json.label.length === 1 || json.label[0] === "f") {
          tapKey(json.label, json.combo)
        } else {
          robot.typeString(json.label)
        }
        return true
    }
  }

  return false
}