const robot = require('robotjs')

module.exports = function (req, res, json) {
  if (json.mouse) {
    switch (json.mouse) {
      case 'click':
        robot.mouseClick('left')
        return true
      case 'double_click':
        robot.mouseClick('left', true)
        return true
      case 'move':
        if (json.move) {
          let mouse = robot.getMousePos()
          robot.moveMouse(mouse.x + json.move.x, mouse.y + json.move.y)
        }
        return true
    }
  }

  return false
}