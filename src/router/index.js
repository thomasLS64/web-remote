import Vue from 'vue'
import VueRouter from 'vue-router'
import store from '../store/index.js'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: () => import('../views/Home.vue')
  },
  {
    path: '/remote',
    name: 'Remote',
    meta: { requireAuthentication: true },
    component: () => import('../views/Remote.vue')
  },
  {
    path: '/list',
    name: 'AppList',
    meta: { requireAuthentication: true },
    component: () => import('../views/AppList.vue')
  },
  {
    path: '/users',
    name: 'UserManagement',
    meta: { requireAuthentication: true },
    component: () => import('../views/UserManagement.vue')
  },
  {
    path: '/info',
    name: 'NodeInfo',
    meta: { requireAuthentication: true },
    component: () => import('../views/NodeInfo.vue')
  },
  {
    path: '/finder',
    name: 'Finder',
    meta: { requireAuthentication: true },
    component: () => import('../views/Finder.vue')
  },
  {
    path: '/keyboard',
    name: 'Keyboard',
    meta: { requireAuthentication: true },
    component: () => import('../views/Keyboard.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requireAuthentication) && !store.state.username) {
    next({ name: 'Home' })
  } else {
    next()
  }
})

export default router
