import Vue from 'vue'
import Vuex from 'vuex'
import API from "../../app/Helper/API.js"

Vue.use(Vuex)

const defaultState = {
  username: undefined,
  role: undefined,
  apps: undefined,
  users: undefined,
  firstConnexion: undefined
}

export default new Vuex.Store({
  state: defaultState,
  mutations: {
    authenticate(state, user) {
      if (user && user.username) {
        state.username = user.username
        state.role = user.role
        state.firstConnexion = user.firstConnexion
        Vue.set(state, 'apps', user.apps)

        if (user.users) {
          Vue.set(state, 'users', user.users)
        }
      }
    },
    addUser(state, user) {
      state.users.push({
        username: user.username,
        role: user.role
      })
    },
    addApp(state, app) {
      state.apps.push(app)
    }
  },
  actions: {
    async authenticate({ commit }, credential) {
      let url = window.location.origin.split(':')

      url.pop()
      url = url.join(':')
      this.error = undefined

      try {
        const res = await API.fetchWebServer({
          path: '/api/v1/auth',
          method: 'POST',
          body: credential
        })

        if (200 === res.status) {
          const json = await res.json()
          commit('authenticate', json.data)
          window.sessionStorage.setItem('user', JSON.stringify(credential))

          return { authenticate: true, firstConnexion: json.data.firstConnexion }
        } else {
          return { error: 'User not found' }
        }
      } catch (error) {
        return { error: error }
      }
    },
    async autoLogIn({ dispatch }) {
      let credential = window.sessionStorage.getItem('user')

      if (!credential) {
        credential = window.localStorage.getItem('user')
      }

      if (credential) {
        credential = JSON.parse(credential)
        return await dispatch('authenticate', credential)
      }

      return false
    },
    logOut(context) {
      Vue.set(context, 'state', defaultState)
      window.sessionStorage.removeItem('user')
      window.localStorage.removeItem('user')
    }
  },
  modules: {
  }
})
