# web-remote
Easily manipulate your node

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run dev
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Start production application
```
npm run serve
```
Default user is `admin` / `password`
