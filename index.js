
// Prepare constants
const optsHttpServer = {
  host: '0.0.0.0',
  port: 8081
}

// Start HTTP Server
require('./app/Server/HTTPServer.js')(optsHttpServer)
